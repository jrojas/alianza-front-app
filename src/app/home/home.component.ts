import { Component, OnInit } from '@angular/core';
import { HomeService } from '../services/home.service';
import { ClientService } from '../services/client.service';
import { Client } from './clients';
import Swal from 'sweetalert2'
import { Observable } from 'rxjs';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent implements OnInit {

  clients: Client[]=[];
  title:string="Create Client";
  nameButton:String="Add";
  isCreate:Boolean=true;
  clientDat:Client=new Client();
  sharedKey:string="";

  constructor(private homeService:HomeService, private clientService:ClientService){
    this.findClients();
  }
  

  ngOnInit(): void {
    this.findClients();
  }

  public findClients():void{
    this.clientService.findClients().subscribe(
      clients => this.clients= clients
    );
  }

  public findClientsBySharedKey():void{
    this.clientService.findClientsBySharedKey(this.sharedKey).subscribe(
      clients => this.clients= clients
    );
  }

  public createClient():void{
    this.clientService.createClient(this.clientDat).subscribe(
      response => {
        this.clientDat=new Client();
        this.findClients();
        Swal.fire({
          title: 'Success!',
          text: 'Client create success',
          icon: 'success'
        })

      }  
    );
    console.log("Creado");
    console.log(this.clientDat);
  }

  public editClient():void{
    this.clientService.editClient(this.clientDat).subscribe(
      response => {
        this.clientDat=new Client();
        this.findClients();
        Swal.fire({
          title: 'Success!',
          text: 'Client Edit success',
          icon: 'success'
        })

      }  
    );
    console.log("Creado");
    console.log(this.clientDat);
  }

  public getClient(id:string):void{
    this.title="Edit Client";
    this.isCreate=false;
    this.nameButton="Edit";
    this.clientService.getClient(id).subscribe(
      client => this.clientDat= client
    );
  }

  export(): void {
    this.clientService.export().subscribe(
      (excelData: Blob) => {
        const blob = new Blob([excelData], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        const url = window.URL.createObjectURL(blob);
        window.open(url);
      },
      (error) => {        
        console.error('rror download expor', error);
        Swal.fire({
          title: 'Error!',
          text: 'Error download export',
          icon: 'error'
        })
      }
    );
  }

}
