import { Injectable } from '@angular/core';
import { Client } from '../home/clients';
import { of,Observable,map } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private endPoint:string ="http://localhost:9090/alianza-services/api/client/"
  private endPointList:string =this.endPoint+"list"  
  private endPointCreate:string =this.endPoint+"create"
  private endPointEdit:string =this.endPoint+"edit/";
  private endPointExport:string =this.endPoint+"export"
  private endPointShared:string =this.endPoint+"sharedKey/"

  private httpHeaders =new HttpHeaders({'content-type':'application/json'})
  constructor(private httpRest:HttpClient) { }

  /**
   * 
   * @returns Lista de Todos lo Clientes
   */
  findClients(): Observable<Client[]>{  
  return this.httpRest.get(this.endPointList).pipe(
    map(response => response as Client[])
   );
  }

  /**
   * Servico que retorna todos los clientes que posean un sharedKey
   * @param sharedKey 
   * @returns 
   */
  findClientsBySharedKey(sharedKey: string): Observable<Client[]>{
    return this.httpRest.get(this.endPointShared+sharedKey).pipe(
      map(response => response as Client[])
     );
    }

    /**
     * Servicio que permite la creacion de clientes
     * @param client 
     * @returns 
     */
  createClient(client: Client): Observable<Client>{
    return this.httpRest.post<Client>(this.endPointCreate,client, {headers: this.httpHeaders});
  }

  /**
   * Servicio que permite la actualizacion de clientes
   * @param client 
   * @returns 
   */
  editClient(client: Client): Observable<Client>{
    return this.httpRest.put<Client>(this.endPointEdit+client.id,client, {headers: this.httpHeaders});
  }


  /**
   * Servicio que permite consultar un clientre por Id
   * @param id 
   * @returns 
   */
  getClient(id:string) : Observable<Client>{
    return this.httpRest.get(this.endPoint+id).pipe(
      map(response => response as Client)      
     );
  }

  /**
   * Servicio que permite exportar un archivo excel con la informacion de los clientes
   * @returns 
   */
  export(): Observable<Blob> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.httpRest.get(this.endPointExport, {
      headers,
      responseType: 'blob',
    });
  }
}
